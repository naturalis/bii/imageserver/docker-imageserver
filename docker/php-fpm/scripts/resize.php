<?php
ini_set("memory_limit","80M");

# figure out requested path and actual physical file paths
$orig_dir = dirname(__FILE__);
$orig_dir = '/data';
$path = $_GET['path'];

list($prepend, $size, $file) = explode("/", $path, 3);
$orig_file = $orig_dir."/original/".$file;
 
if (!file_exists($orig_file))
{
    http_response_code(404);
    error_log("Image does not exist: $orig_file");
    die();
}

$save_path = ($prepend != "") ? $orig_dir . "/" . $prepend : $orig_dir;
$save_path = "$save_path/$size/$file";

# parse out the requested image dimensions and resize mode
$x_pos = strpos($size, 'x');
$target_width = substr($size, 0, $x_pos);
$target_height = substr($size, $x_pos+1, $x_pos);
$new_width = $target_width;
$new_height = $target_height;

# default mode
$mode = "2";

# specific mode settings and size settings based on URL

if ($size == "comping"){
  $new_width = 500;
  $new_height = 500;
  $target_width = 500;
  $target_height = 500;
  $mode = "0";
}

if ($size == "w800"){
  $new_width = 800;
  $new_height = 800;
  $target_width = 800;
  $target_height = 800;
  $mode = "0";
}

if ($size == "w1200"){
  $new_width = 1200;
  $new_height = 1200;
  $target_width = 1200;
  $target_height = 1200;
  $mode = "0";
}

$image = new Imagick($orig_file);
$format = $image->getImageFormat();
$mimetype = $image->getImageMimeType();

list($orig_width, $orig_height, $type, $attr) = getimagesize($orig_file);
 
# preserve aspect ratio, fitting image to specified box
if ($mode == "0")
{
        $new_height = $orig_height * $new_width / $orig_width;
        if ($new_height > $target_height)
        {
                $new_width = $orig_width * $target_height / $orig_height;
                $new_height = $target_height;
        }
}
# zoom and crop to exactly fit specified box
else if ($mode == "2")
{
        // crop to get desired aspect ratio
        $desired_aspect = $target_width / $target_height;
        $orig_aspect = $orig_width / $orig_height;
 
        if ($desired_aspect > $orig_aspect)
        {
                $trim = $orig_height - ($orig_width / $desired_aspect);
                $image->cropImage($orig_width, floor($orig_height-$trim), 0, floor($trim/2));
        }
        else
        {
                $trim = $orig_width - ($orig_height * $desired_aspect);
                $image->cropImage(floor($orig_width-$trim), $orig_height, floor($trim/2), 0);
        }
}

# mode 3 (stretch to fit) is automatic fall-through as image will be blindly resized
# in following code to specified box
try {
    $image->resizeImage(floor($new_width), floor($new_height), imagick::FILTER_LANCZOS, 1);
} catch (Exception $e) {
    error_log($e->getMessage());
    http_response_code(500);
    die();
}

try {
    if (!file_exists(dirname($save_path)))
        mkdir(dirname($save_path), 0755, true);

    if ($format == 'JPEG') {
        $image->setImageCompression(imagick::COMPRESSION_JPEG);
        $image->setImageCompressionQuality(80);
    }
    $image->writeImage($save_path);
} catch (Exception $e) {
    error_log($e->getMessage());
    http_response_code(500);
    die();
}

header('Content-type: ' . $mimetype);

echo file_get_contents($save_path);
