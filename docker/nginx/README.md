Nginx Dockerfile
================

This build the nginx part of the imageserver setup.
The webserver serves the images as static content.
Whenever a thumbnail is not found it is generated by
[php-fpm/scripts/resizes.php](../php-fpm/scripts/resize.php).
