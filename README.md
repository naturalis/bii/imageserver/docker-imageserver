docker-imageserver
==================
Docker components and docker-compose definition for deployment of nginx based 
image server. Image server is used for serving images for:

- [nederlandsesoorten.nl](https://www.nederlandsesoorten.nl),
- [determineren.nederlandsesoorten.nl](https://determineren.nederlandsesoorten.nl), 
- [dutchcaribbeanspecies.org](https://www.dutchcaribbeanspecies.org/) and
- [grasshopperofeurope.com](https://www.grasshoppersofeurope.com/)

Images and other media are uploaded by scp (ssh secure copy) and using minio clients
or the minio web interface and copied to the webserver part of this configuration.

Image published by the *beelduitwisselaars* of nsr and csr upload the files
by scp whenever the images are published. This is done by linnaeus configurations
which have `image_upload: true` in the configuration. See
[image_upload.yml](https://gitlab.com/naturalis/bii/linnaeus/linnaeus_ng/-/blob/develop/ansible/roles/deploy/tasks/image_upload.yml) of Linnaeus.
Installations that have this are:

 - [nederlandsesoorten.nl/linnaeus_ng](https://www.nederlandsesoorten.nl/linnaeus_ng/)
 - [dutchcaribbeanspecies.org/linnaeus_ng](https://www.dutchcaribbeanspecies.org/linnaeus_ng/)

Docker-compose
--------------

This is a complete docker-compose setup for an imageserver, which consists of:

 - nginx
 - php-fpm (for imagemagick)
 - traefik
 - minio

It will be deployed using ansible which creates:

 - data directories
 - complete docker-compose setup
 - .env file
 - services and timers

Hilights of this setup
----------------------

 - [nginx configuration](docker/nginx/config/default.conf)
 - [php resize script](docker/php-fpm/scripts/resize.php)
 - [ansible role: handle incoming](ansible/roles/deploy/tasks/handle-incoming.yml)
 - [handle_incoming.sh](ansible/roles/deploy/files/handle_incoming.sh)


Local development installation
------------------------------

1. Clone this repo
2. Create a .env file with the AWS secrets from Bitwarden (look for *traefik-route53-dryrun.link*).
3. Add the domain to your local `/etc/hosts`

```bash
echo "127.0.0.1  images.dryrun.link  minio.images.dryrun.link  minio-console.images.dryrun.link" >> /etc/hosts
```

4. Create directory `mkdir -p ./data/www/original/;chmod -R 777 data/www`
5. `cp ansible/roles/deploy/files/testbeeld.png data/www/original`
6. `docker-compose up`
7. Start a browser and open https://images.dryrun.link/testbeeld.png
8. Open https://images.dryrun.link/comping/testbeeld.png

Testing or changing the ansible deployment
------------------------------------------

Install [virtualbox](https://www.virtualbox.org/) and [vagrant](https://www.vagrantup.com/).

1. Clone the [ansible-imageserver repo](https://gitlab.com/naturalis/bii/imageserver/ansible-imageserver)
2. Move into the clone environment and `vagrant up`
3. Wait for the machine to provision, move back to this repo
4. `IMAGE_VERSION=latest ansible-playbook -i ansible/inventory/hosts.ini ansible/deploy.yml -l local`
5. Add the host to the `/etc/hosts` file

```
echo "127.0.0.1  images.dryrun.link  minio.images.dryrun.link  minio-console.images.dryrun.link" >> /etc/hosts
```

6. After running ansible you see an image at https://images.dryrun.link:8081/testbeeld.png

## Trivyignore

The gitlab pipeline checks the security of the build docker images using
[trivy](https://trivy.dev/).
Whenever trivy throws up security issues that stop the pipeline but
can't be fixed yet you can add a CVE number of the issue to the
ignore file.
This process has been centralized in
[lib / trivyignore](https://gitlab.com/naturalis/lib/trivyignore),
you should follow the
[instructions](https://gitlab.com/naturalis/lib/trivyignore#procedure) in the project.

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

- `pre-commit autoupdate`
- `pre-commit install`

