#!/bin/sh
set -eu

basedir=$(dirname "$0")
php_fpm="$CI_REGISTRY_IMAGE/php-fpm:latest"
php_dev="$CI_REGISTRY_IMAGE/php-dev:latest"

if docker pull "$php_fpm"; then
  if ! docker run --rm "$php_fpm" sh -c \
    'if ! apk list --no-cache --upgradable | grep upgradable; then
      exit 0
    fi
    exit 1'
  then
    docker image rm "$php_fpm"
  else
    docker pull "$php_dev" || true
  fi
fi

docker build --target fpm --pull --cache-from "$php_fpm" \
  --tag "$CI_REGISTRY_IMAGE/php-fpm:build_$CI_COMMIT_REF_SLUG" -f "$basedir/php-fpm/Dockerfile" .
docker build --target dev --cache-from "$CI_REGISTRY_IMAGE/php-fpm:build_$CI_COMMIT_REF_SLUG" \
  --cache-from "$php_dev" --tag "$CI_REGISTRY_IMAGE/php-dev:build_$CI_COMMIT_REF_SLUG" -f "$basedir/php-fpm/Dockerfile" .

docker push "$CI_REGISTRY_IMAGE/php-fpm:build_$CI_COMMIT_REF_SLUG"
docker push "$CI_REGISTRY_IMAGE/php-dev:build_$CI_COMMIT_REF_SLUG"
