#!/bin/bash
INCOMING=/data/incoming/images
IMAGE_ROOT=/data/www
TARGET=/data/www/original
NOW=$( date +"%Y-%m-%d %H:%M:%S" )

# if empty incoming directory, silent exit
if [ ! "$(ls -A $INCOMING)" ]; then
  exit
fi

echo "+ Copying incoming files - start @ $NOW"

echo "| cleaning up before moving"
find $INCOMING/ -type f | while read -r f; do
  if [[ -f $f ]]; then
    # file
    # ${f,,} is the filename in lowercase
    if [[ ! ${f,,} == *.jpeg && ! ${f,,} == *.jpg && ! ${f,,} == *.png ]]; then
      echo "deleting file of unsupported type $f"
      rm "$f"
    else
      echo "found $f"
    fi
    elif [ ! -d "$f" ]; then
      # something else, not dir
      echo "deleting incorrect file '$f'"
      rm "$f"
    fi
done


# if empty incoming directory after clean up, exit
if [ ! "$(ls -A $INCOMING)" ]; then
  printf "| no files to move; exiting\n\n"
  exit
fi


echo "| remove existing resized copies of new files"
for f in "$INCOMING"/* "$INCOMING"/**/* ; do
  if [[ -f $f ]]; then
    FILENAME=$(basename "$f")
	while IFS=$'\n' read -r -d '' file
	do
      echo "found $file"
      if  [[ ! $file == *original/* ]] ;
      then
        echo "| deleting old resized copy $file"
        rm "$file";
      else
        echo "| ignoring original"
      fi
	done <   <(find "$IMAGE_ROOT"/ -name "$FILENAME" -print0)
  fi
done;


echo "| moving files to $TARGET"
cp -rv "${INCOMING}"/* "${TARGET}"/
echo "| removing files from $INCOMING"
find "${INCOMING}" -type f -delete

echo "+ Copying incoming files - done"
