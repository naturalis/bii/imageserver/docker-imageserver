docker-imageserver custom dockerfile for fpm
============================================

This builds a custom docker image which contains imagemagick and a php script to create
image thumbnails.

It's a Dockerfile consisting of two stages which is used to build two images:

 - base php-fpm image
 - and one containing XDebug

The last image can (and should) be used for development purposes.

## Resize

The [resize.php](scripts/resize.php) script is called whenever the webserver 
[cannot find a thumbnail](https://gitlab.com/naturalis/bii/imageserver/docker-imageserver/-/blob/develop/docker/nginx/config/default.conf#L14).
It passes the parameters to the script and uses this to create the
thumbnail. After generating the thumbnail it is stored and handled
as static content.

